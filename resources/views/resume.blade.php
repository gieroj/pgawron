@extends('main')
@section('content')
<div class="container">
    <div class="row">
        <div class="resume">
            <div class="border1">
                <div class="border2">
                    <div class="row">
                        <div class="small-12 columns">
                            <h1 class="text-center">Pawel Gawron</h1>
                            <div class="small-6 columns text-left">
                                773-809-0811
                            </div>
                            <div class="small-6 columns">
                                <p class="text-right"><a href="mailto:gieroj@gmail.com" class="right" >gieroj@gmail.com</a></p>
                            </div>
                            <div class="small-12 columns border_bottom">
                                EDUCATION
                            </div>
                            <ul>
                                <p><span class="tetx-normal">&#10070</span>  Engineer of Science in Information Technology 
                                    <span class="tetx-normal">Specialty: Information Technology in the Enterprise, Rzeszow University of Technology, Poland May 2010 - 2014 (Not Completed)</span></p>
                            </ul>
                            <div class="small-12 columns border_bottom">
                                WEB DEVELOPER/ DESIGNER EXPERIENCE:
                            </div>
                            <ul> 
                                <div class="row">
                                    <div class="small-8 columns"><span class="tetx-normal">&#10070</span> Website Developer mRELEVANCE (Contract)</div>
                                    <div class="small-4 columns text-right">August 2015 –  Current</div>
                                </div>
                                <li>
                                    Creating WordPress and rAdmin responsive design from the scratch  (HTML, SCSS, CSS, jQuery, Foundation). 
                                </li>
                                <li>
                                    Adding new functionality to existing website by PHP exp.(payment method,  house importer)
                                </li>
                                <li>
                                    Custom PHP Framework
                                </li>
                                <li>
                                    Linux bash
                                </li>
                                <li>
                                    Version control GIT
                                </li>
                                <li>
                                    MySQL, Ajax, NetBeans, Navicat,  Wordpress, Gulp
                                </li>
                            </ul>
                            <ul> 
                                <div class="row">
                                    <div class="small-8 columns"><span class="tetx-normal">&#10070</span> Website Developer mRELEVANCE (Contract)</div>
                                    <div class="small-4 columns text-right">March 2015 –  June 2015</div>
                                </div>
                                <li>
                                    Editing WordPress template (HTML CSS jQuery). 
                                </li>
                                <li>
                                    Adding new products with pictures and description.
                                </li>
                                <li>
                                    Images cutting and retouch (Photoshop).
                                </li>
                                <li>
                                    Create Brochures and Flyers (Photoshop).
                                </li>
                                <li>
                                    Search Engine Optimization
                                </li>
                            </ul>
                            <ul> 
                                <div class="row">
                                    <div class="small-8 columns"><span class="tetx-normal">&#10070</span> Front End Web Developer Chicago QuickSolution365 (Contract)</div>
                                    <div class="small-4 columns text-right">November 2014 – March 2015</div>
                                </div>
                                <li>
                                    Responsible for coding design concepts into functional web pages utilizing HTML, CSS, PHP, JavaScript, JQuery
                                </li>
                                <li>
                                    Develop cross-browser compatible pages with responsive design.
                                </li>
                                <li>
                                    Managed and uploaded content using a web-based CMS system for clients (WordPress).
                                </li>
                                <li>
                                    Responsible for creating banner advertisements by using Photoshop.
                                </li>
                                <li>
                                    Provide production support services and other functions as necessary or assigned.
                                </li>
                            </ul>
                            <ul> 
                                <div class="row">
                                    <div class="small-8 columns"><span class="tetx-normal">&#10070</span> Web Designer/ Developer Rzeszow Poland Dobry-Dom (Contract)</div>
                                    <div class="small-4 columns text-right">March 2013 – October 2014</div>
                                </div>
                                <li>
                                    Web page design and development by utilizing HTML, CSS, JavaScript, JQuery, MySQL, Laravel, PHP
                                </li>
                                <li>
                                    Develop cross-browser compatible pages with responsive design.
                                </li>
                                <li>
                                    Develop and maintain interactive Web applications and supporting databases to prescribe specifications.
                                </li>
                                <li>
                                    Responsible for creating or editing images and graphics for website use by using Photoshop.
                                </li>
                                <li>
                                    Managed and uploaded content using a web-base CMS system for clients (WordPress, Prestashop, Shoper, Joomla, Magento).
                                </li>
                                <li>
                                    Version control GIT
                                </li>
                                <li>
                                    Linux server management LAMP, Perl, Crontab, Linux shell command
                                </li>
                                <li>
                                    Integration website by: xml, JSON and REST. 
                                </li>
                            </ul>
                            <ul> 
                                <div class="row">
                                    <div class="small-8 columns"><span class="tetx-normal">&#10070</span> MQL developer Freelancer Poland Allegro</div>
                                    <div class="small-4 columns text-right">September 2011 – March 2013</div>
                                </div>
                                <li>
                                    Develop MQL strategy.
                                </li>
                                <li>
                                    Develop cross-browser compatible pages with responsive design.
                                </li>
                                <li>
                                    Create indicators and integration data feds.
                                </li>
                                <li>
                                    Analyze forex market
                                </li>
                            </ul>
                            <div class="small-12 columns border_bottom">
                                TECHNICAL SKILLS
                            </div>
                            <div class="small-12 columns">
                                <span class="tetx-normal">&#10070</span> Languages:<span class="tetx-normal"> HTML5, CSS, SASS, JAVASCRIPT, JQUERY, AJAX, PHP, MySQL, less, bash,  xml, Perl</span> <br>
                                <span class="tetx-normal">&#10070</span> Software: <span class="tetx-normal">Photoshop, GIT, WordPress, Prestashop, Shoper , Netbeans, Navicat, MySQL Workbench, Laravel </span>
                            </div>
                            <div class="small-12 columns border_bottom">
                                PORTFOLIO
                            </div>
                            <div class="small-12 columns">
                                <ul>
                                    <li>
                                        <a href="http://www.bhavt.com" target="_blank" >www.bhavt.com</a>
                                    </li>
                                    <li>
                                        <a href="http://www.chicagometalsupply.com" target="_blank" >www.chicagometalsupply.com</a>
                                    </li>
                                    <li>
                                        <a href="http://www.clachicago.com" target="_blank" >www.clachicago.com</a>
                                    </li>
                                    <li>
                                        <a href="http://www.tada.center" target="_blank" >www.tada.center</a>
                                    </li>
                                    <li>
                                        <a href="http://www.atlantarealestateforum.com" target="_blank" >www.atlantarealestateforum.com</a>
                                    </li>
                                    <li>
                                        <a href="http://www.wnetrzeiogrod.pl" target="_blank" >www.wnetrzeiogrod.pl</a>
                                    </li>
                                    <li>
                                        <a href="http://www.dobra-mama.pl" target="_blank" >www.dobra-mama.pl</a>
                                    </li>
                                    <li>
                                        <a href="http://www.theprovidencegroup.com" target="_blank" >www.theprovidencegroup.com</a>
                                    </li>
                                    <li>
                                        <a href="http://www.dobry-dom.pl" target="_blank" >www.dobry-dom.pl</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="small-12 columns border_bottom">
                                TECHNICAL SKILLS
                            </div>
                            <div class="small-8 columns">
                                <span class="tetx-normal">&#10070</span> Member<span class="tetx-normal">, Interaction between Human and Computer GEST, Rzeszow Poland, </span> <br>
                            </div>
                            <div class="small-4 columns">
                                <p class="text-right">September 2013</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <a href="/images/Pawel_Gawron_Resume.doc" class="button">Download</a>
        <a onclick="window.print();" class="button">Print</a>
    </div>
</div>
@endsection