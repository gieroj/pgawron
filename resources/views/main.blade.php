@include('head')
@include('menu')
@include('footer')
<!DOCTYPE html>
<html>
    @yield('head')
    <body>
        
        @yield('menu')
        @yield('content')
        @yield('footer')
    </body>
</html>
