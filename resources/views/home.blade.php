
@extends('main')
@section('content')
<div class="matrix">
    <div class="row">
        <img id="cool_jesus" src="/images/jesus-is-cool.png" alt="Cool Jesus"/>
        <div class="text-center title hide-for-small-only">Pawel Gawron</div>
    </div>
</div>
<div class="navy_blue">
    <div class="row">
        <div class="small-12 columns">
            
                <span>class</span> Pawel_Gawron{<br>
                <div class="tab"><span>protected</span> <span class="blue">$phone</span> = <span class="yellow">'773-809-0811'</span>;<br></div>
                <div class="tab"><span>protected</span> <span class="blue">$email</span> = <a href="mailto:gieroj@gmail.com?Subject=Hello%20pgawron.com"><span class="yellow">'gieroj@gmail.com'</span></a>;<br></div>
                <div class="tab"><span>private</span> <span class="blue">$age</span> = <span class="yellow">27</span>;<br></div>
                <div class="tab"><span>private</span> <span class="blue">$color</span> = <span class="yellow">'white'</span>;<br></div>
                <br>
                <div class="tab"><span>public</span> <span class="blue">$experience</span> = <span>array</span>(<br></div>
                <div class="double_tab">
                    <span class="yellow">'PHP'</span>,<br>
                    <span class="yellow">'MySQL'</span>,<br>
                    <span class="yellow">'HTML'</span>,<br>
                    <span class="yellow">'CSS'</span> => <span class="yellow">'SASS'</span>, <span class="yellow">'LESS'</span>, 
                         <span class="yellow">'GULP'</span>, <span class="yellow">'GRUNT'</span>
                        , <span class="yellow">'Bootstrap'</span>, <span class="yellow">'Foundation'</span>,<br>
                    <span class="yellow">'JavaScript'</span> => <span class="yellow">'jQuery'</span>, <span class="yellow">'Angular'</span>,<br>
                    <span class="yellow">'System'</span> => <span class="yellow">'LAMP'</span>, <span class="yellow">'Debian'</span>, 
                        <span class="yellow">'Centos'</span>, <span class="yellow">'Composer'</span>,
                         <span class="yellow">'Bower'</span>, <span class="yellow">'Git'</span>,<br>
                    <span class="yellow">'Framework'</span> => <span class="yellow">'Laravel'</span>, <span class="yellow">'CakePHP'</span>,
                         <span class="yellow">'Custom'</span>,<br>
                    <span class="yellow">'CMS'</span> => <span class="yellow">'WordPress'</span>, <span class="yellow">'PrestaShop'</span>,
                         <span class="yellow">'Joomla'</span>, <span class="yellow">'Shoper'</span>, <span class="yellow">'Magento'</span>,<br>
                    <span class="yellow">'Software'</span> => <span class="yellow">'Photoshop'</span>, <span class="yellow">'NetBeans'</span>,
                         <span class="yellow">'MySQL Workbench'</span>, <span class="yellow">'Navicad'</span>, <span class="yellow">'phpMyAdmin'</span>,
                         <span class="yellow">'cPanel'</span>, <span class="yellow">'Plesk'</span>, <span class="yellow">'WHM'</span>,<br>
                </div>
                <div class="tab">);<br></div>
                }<br>
            
        </div> 
    </div>
</div>
<div class="circle_slider">
    
    <div class="row_circle">
        <div class="small-12 medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
        <div class="show-for-medium medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
        <div class="show-for-medium medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
    </div>
    <ul class="bxslider">
        <li><img src="/images/slider/Baner_dormery.jpg" /></li>
        <li><img src="/images/slider/spring-flat.jpg" /></li>
        <li><img src="/images/slider/heli.jpg" /></li>
        <li><img src="/images/slider/roof.jpg" /></li>
    </ul>
</div>
<div class="navy_blue contact_form">
    <div class="row">
        <form>
            <h2>Contact Me</h2>
            <label>
                <input type="text" placeholder="Company"/>
            </label>
            <label>
                <input type="text" placeholder="Email">
            </label>
            <label>
                <input type="text" placeholder="Phone">
            </label>
            <textarea placeholder="Content"></textarea>
            <a href="#" class="button">Send</a>
        </form>
    </div>
    <div class="row_circle_bottom">
        <div class="small-12 medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
        <div class="small-12 medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
        <div class="small-12 medium-4 columns">
            <a href="/portfolio/chicago-metal">
                <div class="circle">
                    <h2>Chicago Metal Supply</h2>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection
