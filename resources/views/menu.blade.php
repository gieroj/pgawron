

@section('menu')
<div class="top_menu">
    <ul class="menu expanded">
        <li><a href="/about">About</a></li>
        <li><a href="/resume">Resume</a></li>
        <li class="menu_star"><a href="/"><div id="burst-12"><img src="/images/logo.png" alt="Logo" /></div></a>
        </li>
        <li><a href="/portfolio">Portfolio</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
</div>
@endsection