
@extends('main')
@section('content')
<div class="container">
    <div class="row ">

        <form>
            <h2>Contact Me</h2>
            <label>
                <input type="text" placeholder="Company"/>
            </label>
            <label>
                <input type="text" placeholder="Email">
            </label>
            <label>
                <input type="text" placeholder="Phone">
            </label>
            <textarea placeholder="Content"></textarea>
            <a href="#" class="button">Send</a>
        </form>
    </div>
</div>
@endsection
