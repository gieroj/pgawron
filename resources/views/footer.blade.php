@section('footer')

<div class="footer">
    <div class="row">
        <div class="small-6 columns text-left"><a href="/" >pgawron.com</a></div>
        <div class="small-6 columns text-right"><p>Copyright Pawel Gawron</p></div>
    </div>
</div>
<script src="/js/vendor/jquery.min.js"></script>
        <!--<script src="/js/vendor/what-input.min.js"></script>-->
<script src="/js/vendor/jquery.bxslider.js"></script>
<script src="/js/vendor/foundation.min.js"></script>
<script src="/js/app.js"></script>
@endsection